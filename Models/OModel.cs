﻿namespace TestTask7.Models
{
    public class OModel
    {
        public int? pk_users_id { get; set; }
        public string? name { get; set; }
        public List<TasksModel>? tasks { get; set; }

    }
}
