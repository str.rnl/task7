﻿namespace TestTask7.Models
{
    public class OutputModel
    {
        public int? pk_users_id { get; set; }
        public string? name { get; set; }
        public List<TasksModel>? tasks { get; set; }

    }
}
