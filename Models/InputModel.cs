﻿namespace TestTask7.Models
{
    public class InputModel
    {
        public string name { get; set; }

        public List<TasksModel>? tasks { get; set; }
    }
}
